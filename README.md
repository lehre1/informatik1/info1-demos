# Info1-Demos

## Was ist das?

   Die meisten auf den Folien gezeigten Quelltexte sind im Skript
   verlinkt. Vieles stammt ursprünglich von 
   <https://introcs.cs.princeton.edu/java/code/ > und ist für diese
   Vorlesung angepasst.
    
   Für die Vorlesungsdemos werden mitunter veränderte Versionen 
   verwendet oder auch während der Vorlesung Änderungen vorgenommen,
   manchmal auch zusätzliche Beispiele gegeben.     
   Wer die Beispiele genauer studieren, oder damit experimentieren
   möchte, kann sich hier umsehen: dieses Repository enthält 
   jeweils die nach Vorlesungsende vorliegende Version. 


## Benutzung

   Durch einfaches Navigieren in der
   [Verzeichnisstruktur](https://gitlab.gwdg.de/lehre1/informatik1/info1-demos/-/tree/main)
   kann man die einzelnen Dateien auffinden und herunterladen (Button
   mit Download-Symbol jeweils ganz rechts in der Zeile mit dem
   Datei-Namen) um damit zu experimentieren. Wer sich mit `git`
   auskennt, kann das Repository natürlich auch clonen und von Zeit zu
   Zeit aktualisieren.


    
