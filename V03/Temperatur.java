public class Temperatur{
  public static void main(String[] args) {
    System.out.print("Gradzahl: "); 
    double t;
    t = StdIn.readDouble(); // einlesen
    double tf,tc;
    // Umrechnungsformeln
    tf = t * 1.8 + 32; // °C -> °F
    tc = (t - 32)*5/9; // °F -> °C
    System.out.println(t + " °C = "
                       + tf + " °F.");
    System.out.println(t + " °F = " 
		         + tc + " °C.");
  }
}
