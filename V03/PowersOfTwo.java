public class PowersOfTwo {
    public static void main (String[] args) {
	int n=Integer.parseInt(args[0]), p=1;
	int i;                    // <-- Laufvariable
	for ( i=0; i<=n; i=i+1) { // <-- for-Anweisung: *Kopf* und .. 
            System.out.println(i + " " + p); //             \  
            p=2*p;                           //              >  .. *Rumpf* 
        }                                    //             /
    }
}
