public class Echo {
    public static void main(String[] args) {
        int n = args.length;
        int i = 0;
        while (i < n) {
            System.out.print(args[i] + " ");
            i = i+1;
        }
        System.out.println();
    }
}
