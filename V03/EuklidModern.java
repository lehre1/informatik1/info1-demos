public class EuklidModern {
    public static void main(String[] args) {
	int a = Integer.parseInt(args[0]), b = Integer.parseInt(args[1]);
	int x = a, y = b;      
	int r;
	while ( y != 0) { 
	    r = x % y;  
	    x = y; 
	    y = r;
	}
	System.out.println(x); 
    }
}


