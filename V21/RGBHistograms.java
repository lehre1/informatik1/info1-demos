import java.awt.Color;
public class RGBHistograms {
    public static void main(String[] args) {
        Picture source = new Picture(args[0]);
        source.show(); 
        int W = source.width(), H = source.height();
        Histogram redHist = new Histogram(256);
        Histogram greenHist = new Histogram(256);
        Histogram blueHist = new Histogram(256);
        for (int w = 0; w < W; w++) {
            for (int h = 0; h < H; h++) {
                Color c = source.get(w,h);
                redHist.addDataPoint(c.getRed());
                greenHist.addDataPoint(c.getGreen());
                blueHist.addDataPoint(c.getBlue());
            }
        }
        // StdDraw.setCanvasSize(256,256);
        StdDraw.setPenColor(StdDraw.RED);
        redHist.draw();
        StdDraw.setPenColor(StdDraw.GREEN);
        greenHist.draw();
        StdDraw.setPenColor(StdDraw.BLUE);
        blueHist.draw();
    }
}
