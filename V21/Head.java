public class Head {
    public static void main(String[] args) {
        String name = args[0]; 
        In in = new In(name);     
        int max = 10;
        for (int i = 0; i < 10 && !in.isEmpty(); i++) {
            String line = in.readLine();
            System.out.println(line);
        }
    }
}

/*
public class Head {
    public static void main(String[] args) {
        In in = new In(); // default: von Standardeingabe lesen
        if (args.length > 0) 
            String name = args[args.length-1]; 
        in = new In(name);     
        int max = 10;
        if (args.length == 2) // interpretiere Argument als Zeilenzahl
            max = Integer.parseInt(args[0]);
        int z = 0; 
        while(z < max && !in.isEmpty()) {
            String line = in.readLine();
            System.out.println(line);
            z++;
        }       
    }
}
*/
