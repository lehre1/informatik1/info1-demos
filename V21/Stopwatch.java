/******************************************************************************
 *  Compilation:  javac Stopwatch.java
 *  Execution:    none
 *  Dependencies: none
 *
 *  A utility class to measure the running time (wall clock) of a
 *  program.
 *
 ******************************************************************************/

/**
 *  The <tt>Stopwatch</tt> data type is for measuring
 *  the time that elapses between the start and end of a
 *  programming task (wall-clock time).
 *
 *  See {@link StopwatchCPU} for a version that measures CPU time.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
// http://introcs.cs.princeton.edu/java/stdlib/Stopwatch.java.html
// leicht veraendert
public class Stopwatch { 
    private final long start;
    public Stopwatch() { start = System.currentTimeMillis(); } 
    public double elapsedTime() {
        long now = System.currentTimeMillis();
        return (now - start) / 1000.0;
    }
    
    /* Test-Client:
       Aufruf 'java Stopwatch <ganze Zahl>'
       - misst Zeit zum Berechnen von sqrt(1) + ... + sqrt(N)
         wobei die Quadratwurzel einmal mittels Math.sqrt und einmal 
	 mittels Newton.sqrt berechnet wird. 
    */
    public static void main(String[] args) {
	int N = Integer.parseInt(args[0]);
	double totalMath = 0.0; 
	Stopwatch swMath = new Stopwatch();
	for (int i = 1; i <= N; i++) 
	    totalMath += Math.sqrt(i);
	double timeMath = swMath.elapsedTime();

	double totalNewton = 0.0; 
	Stopwatch swNewton = new Stopwatch();
	for (int i = 1; i <= N; i++) 
	    totalNewton += Newton.sqrt(i);
	double timeNewton = swNewton.elapsedTime();
	if (N > 0) { // Ausgabe
            // Quotient der Summen (sollte 1.0 sein fuer N > 0): 
	    System.out.println(totalNewton/totalMath);
            // Quotient der Laufzeiten (zeigt, wieviel schneller die 
            // die Java-Bibliotheksmethode ist als die selbst programmierte)
	    System.out.println(timeNewton/timeMath);
	}
    }
} 
