import java.awt.Color;
public class Histogram {
    private double[] freq;// freq[i] = absolute Haeufigkeit von Wert i
    private double max;  // maximale Haeufigkeit (fuer vertikale Skalierung)

    public Histogram(int n) { freq = new double[n]; } // Histogramm für die Werte {0, 1, .., n-1}
    public void addDataPoint(int i) {  
        freq[i]++; 
        if (freq[i] > max) max = freq[i]; 
    } 
    public void draw() {   
        StdDraw.setYscale(-1, max + 1);  
        StdStats.plotBars(freq); // double[]-Argument erforderlich!
    }
    public static void main(String[] args) {
        int N = Integer.parseInt(args[0]); 
        Histogram histogram = new Histogram(N+1); // Wertebereich [0, 1, ..., N-1]
        int T = Integer.parseInt(args[1]); 
        for (int t = 0; t < T; t++) // simuliere T Bernoulli-Versuche mit
            histogram.addDataPoint(Bernoulli.binomial(N)); // .. N Muenzen
        histogram.draw();
    }
}

