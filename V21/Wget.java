public class Wget {
    public static void main(String[] args) {
        for (int i = 0; i < args.length; i++) {
            String page = args[i];
            if ((page.startsWith("http://") || page.startsWith("https://")) &&
                page.endsWith("html")) {
                In in = new In(page);
                String[] parts = page.split("/");
                Out out = new Out(parts[parts.length-1]); // z.B. "index.html"
                String s = in.readAll();
                out.println(s);
                System.out.println(page); // Erfolgsmeldung
            }
        }
    }
}
