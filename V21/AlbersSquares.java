/* Beispielaufruf: echo 9 90 166 94 94 94 | java AlbersSquares */
import java.awt.Color;
public class AlbersSquares {
    public static void main(String[] args) {
	Color
            c0 = new Color(StdIn.readInt(),StdIn.readInt(),StdIn.readInt()),
            c1 = new Color(StdIn.readInt(),StdIn.readInt(),StdIn.readInt());
        Draw d0 = new Draw(), d1 = new Draw();

        d0.setPenColor(c0); d0.filledSquare(0.5,0.5,0.5);
        d0.setPenColor(c1); d0.filledSquare(0.5,0.25,0.25);
        d0.setPenColor(c0); d0.filledSquare(0.5,0.15,0.15);
        d0.setPenColor(c1); d0.filledSquare(0.5,0.10,0.10);
        d0.show(); d0.save("/tmp/pict0.png");

        d1.setPenColor(c1); d1.filledSquare(0.5,0.5,0.5);
        d1.setPenColor(c0); d1.filledSquare(0.5,0.25,0.25);
        d1.setPenColor(c1); d1.filledSquare(0.5,0.15,0.15);
        d1.setPenColor(c0); d1.filledSquare(0.5,0.10,0.10);
        d1.show(); d1.save("/tmp/pict1.png");

	Draw both = new Draw();
	both.picture(0.5,0.75,"/tmp/pict0.png",1,0.5);     // gestaucht
	both.picture(0.5,0.25,"/tmp/pict1.png",1,0.5,180); // gestaucht+gedreht
	both.show();
    }
}
