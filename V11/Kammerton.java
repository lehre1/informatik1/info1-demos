/* Nach Sedgewick/Wayne */ 
public class Kammerton {
    public static void main(String[] args) {
	int sps = 44100;                // Samples pro Sekunde
	int hz = 440;                   // Frequenz
	double duration = 1.0;          // 5 Sekunden
	int N = (int) (sps * duration); // Gesamtzahl der Samples
	double[] a = new double[N+1];
	for (int i = 0; i <= N; i++) 
	    a[i] = Math.sin(2 * Math.PI * hz * i / sps);
	StdAudio.play(a);
    }
}
