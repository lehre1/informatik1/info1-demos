// Aufrufmöglichkeiten
// java PlotFilter < data.txt # Standardleinwandgröße 512x512
// java PlotFilter 256 256 < data.txt # wählbare Leinwandgröße
// java PlotFilter 256 256 "/tmp/Berlin.png" < # Hintergrund 
public class PlotFilter {
    public static void main(String[] args) {
	if ( args.length == 1) {
	    int m = Integer.parseInt(args[0]);
	    StdDraw.setPenRadius(m * 0.005);
	}
        else if ( args.length > 1 ) {
            int px = Integer.parseInt(args[0]);
            int py = Integer.parseInt(args[1]);
            StdDraw.setCanvasSize(px,py);
        }
        if (args.length > 2) {
            StdDraw.picture(0.5,0.5,args[2]);
        }
	// lies erste 4 Werte der Standardeingabe:
        
	double x0 = StdIn.readDouble(); // x (untere ..
	double y0 = StdIn.readDouble(); // y .. Schranke)
	double x1 = StdIn.readDouble(); // x (obere ..
	double y1 = StdIn.readDouble(); // y .. Schranke)
	// skaliere damit die Leinwand
	StdDraw.setXscale(x0,x1);
	StdDraw.setYscale(y0,y1);
	// solange Eingaben vorhanden ...
	while (!StdIn.isEmpty()) {
	    // lies paarweise und zeichne den Punkt
	    double x = StdIn.readDouble();
	    double y = StdIn.readDouble();
            
	    StdDraw.point(x,y);
	}
    }  
}
