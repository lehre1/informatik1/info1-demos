// siehe: http://introcs.cs.princeton.edu/java/15inout/FunctionGraph.java

/* Erzeugt den Funktionsgraphen y = sin(4x) + sin(20x) im Intervall [0,pi]
   durch Plotten von n Strecken (n als Kdozeilenargument angegeben) */
/* zeigt auch Anzahl der Interpolationspunkte und Koordinatenachsen */

public class FunctionGraph {
    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);                 
        double[] x = new double[n+1], y = new double[n+1]; // Endpunkte ..
        for (int i = 0; i <= n; i++) {     // .. von n Segmenten:
            x[i] = Math.PI * i / n;       // Koordinaten auf x-Achse und ..
            y[i] = Math.sin(4*x[i]) + Math.sin(20*x[i]); // .. y-Achse
        }
        StdDraw.setXscale(0, Math.PI);    
        StdDraw.setYscale(-2.0, +2.0);
        StdDraw.text(Math.PI/2,1.9,(n+1) + " Punkte");
        StdDraw.line(0.,-2.,0.,2.);       // y-Achse
        StdDraw.line(0.,0.,Math.PI,0.);   // x-Achse
        StdDraw.setPenColor(StdDraw.BLUE);
        for (int i = 0; i < n; i++) {    // alle Segmente darstellen
            StdDraw.line(x[i], y[i], x[i+1], y[i+1]);
        }
    }
}
