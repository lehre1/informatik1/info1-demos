public class Sortieren {
    public static void main(String[] args) {
	int n = StdIn.readInt();
	double[] f = new double[n];     
	for (int i = 0; i < n; i++)     
	    f[i] = StdIn.readDouble();  
	java.util.Arrays.sort(f);       // <--FQN-Aufruf der Bibliotheksmethode
	for (int i = 0; i < n; i++)     
	    System.out.println(f[i]);   
    }
}
/* 
import java.util.*;
public class Sortieren {
      ...
	Arrays.sort(f);                 // verkuerzter Aufruf 
      ...
}
*/
