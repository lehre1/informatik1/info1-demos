public class Aa {
    public static void main(String[] args) {
	int sps = 44100;                // Samples pro Sekunde
	int hz = 440;                   // Frequenz
	double duration = 1.0;          // 5 Sekunden
	int N = (int) (sps * duration); // Gesamtzahl der Samples
	double[] a = new double[N+1];
        int primes = 1;
        if (args.length > 0) {
            primes = Integer.parseInt(args[0]);
        }
        double factor = Math.pow(2,primes-1);
	for (int i = 0; i <= N; i++) 
	    a[i] = Math.sin(2 * Math.PI * (factor*hz) * i / sps);
	StdAudio.play(a);
    }
}
