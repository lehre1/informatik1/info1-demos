/* Kammerton (440Hz) mit gegebener Abtastrate (Samples / sec) 
   0.01 Sekunden abtasten, Samples plotten 

   Aufruf: Sampling <Abtastrate>
*/ 

public class Sampling {
    public static void main(String[] args) {
	int sps = Integer.parseInt(args[0]); // Samples pro Sek
	double dauer = 0.01;
	int N = (int) (sps * dauer);
	double[] a = new double[N+1];
	for (int i = 0; i <= N; i++) {
	    a[i] = Math.sin(2 * Math.PI * 440 * i / sps);
	}
	StdDraw.setPenRadius(0.005);
	StdDraw.setXscale(0,N+1);
	StdDraw.setYscale(-1,1);
	for (int i = 0; i <= N; i++)
	    StdDraw.point(i, a[i]); 
    }
}
