/* Aus [Sedgewick/Wayne] */

public class PlayThatTune {
    public static void main(String[] args) {
        while (!StdIn.isEmpty()) {
            // Tonhoehe (in Bezug auf Kammerton) einlesen
            int pitch = StdIn.readInt();
            // Dauer in Sekunden einlesen
            double duration = StdIn.readDouble();

            double hz = 440 * Math.pow(2, pitch / 12.0);
            int N = (int) (StdAudio.SAMPLE_RATE * duration);
            double[] a = new double[N+1];
            for (int i = 0; i <= N; i++) {
                a[i] = Math.sin(2 * Math.PI * hz * i / StdAudio.SAMPLE_RATE);
            }
            StdAudio.play(a);
        }
    }
}
