public class EnvelopeDemo {
    public static void main(String[] args) {
	int N = 50;
	StdDraw.setXscale(0,N);	StdDraw.setYscale(0,N);
        StdDraw.enableDoubleBuffering();
	while( true) 
	    for (int i = 0; i <= N; i++) {
		StdDraw.clear();
		StdDraw.line(0, N-i, i, 0); 
                // StdDraw.show(200); // deprecated (siehe StdDraw-Doku)
                StdDraw.show();
                StdDraw.pause(200);
	    }
    }
}
