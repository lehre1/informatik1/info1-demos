public class HelloDouble {
    public static void main(String[] args) {
        System.out.println(Double.MAX_VALUE);
        System.out.println(1 + Double.MAX_VALUE);
        System.out.println(2 * Double.MAX_VALUE);
        // System.out.println(Double.MAX_VALUE - Double.MAX_VALUE);
        System.out.println(Double.MIN_VALUE);
        System.out.println(Double.MIN_VALUE/2);
        // Division durch 0:
        System.out.println(1.0 / 0);
    }
}
