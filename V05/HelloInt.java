public class HelloInt {
    public static void main(String[] args) {
        System.out.println("int-Literale");
        System.out.println("42:\t" + 42);      // dezimal
        System.out.println("042:\t" + 042);    // oktal
        System.out.println("0x42:\t" + 0x42);  // hexadezimal
        System.out.println("0b101010:\t" + 0b101010);  // dual
        //System.out.println(0b42);  // binär (nur Ziffern 0, 1!)
        //System.out.println(0815);  // oktal (nur Ziffern 0, ..., 7)

        // Integer.MAX_INT:
        System.out.println("0b01111111111111111111111111111111:\t" +
                           (0b01111111111111111111111111111111)
                           );
       
        // Ganzzahlige Division:
        System.out.println("42/8:\t" + (42/8));
        System.out.println("(42/8)*8 + 42 % 8:\t" + ((42/8)*8 + 42 % 8));
       
        // Division durch 0:
        // System.out.println("0 / 0:\t" + (0 / 0));
        System.out.println("0 % 0:\t" + (0 % 0));        
    }
}
