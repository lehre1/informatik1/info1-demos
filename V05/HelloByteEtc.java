public class HelloByteEtc {
    public static void main(String[] args) {
        System.out.println("int-Literale");
        System.out.println("42:\t" + (byte) 42);      // dezimal
        System.out.println("042:\t" + (byte) 042);    // oktal
        System.out.println("0x42:\t" + (byte) 0x42);  // hexadezimal
        System.out.println("0b101010:\t" + (byte) 0b101010);  // dual
        // Subtraktion
        System.out.println("3 - 4 = 3 + (-4) = 0b00000011 + 0b11111100 = \t" +
                           ( 0b00000011 +  0b11111100) );
    }
}
