public class PowersOfTwo_v4 {
    public static void main (String[] args) {
	int n=Integer.parseInt(args[0]);
        double p = 1;
        // Tabellenkopf
        System.out.println("n \t|2 hoch n");
        System.out.println("----------------------");
        // Tabelleneinträge
	for (int i=0; i<=n; i++) {       // <--
            System.out.println(i + "\t|" + p);
	    p=2*p;
	}
    }
}

