public class EVLStack<Item> implements Stack<Item> { 
    private Node first;
    private class Node { // innere Klasse
        Item item;
        Node next;
        Node(Item item, Node next) {
            this.item = item;
            this.next = next;
        }
    }
    public boolean empty() { return ( first == null); }
    public void push(Item item) {
        Node oldfirst = first;
        first = new Node(item,oldfirst);
    }
    public Item pop() {
	if ( empty())
	    throw new RuntimeException("pop von leerem Stack!");
	Item item = first.item; 
        first = first.next;
        return item;
    }
    public Item peek() {
	if ( empty())
	    throw new RuntimeException("peek von leerem Stack!");
        return first.item;
    }
    public String toString() {
	String t = "";
        if (first != null) {
            Node iter = first;
            while( iter != null) {
                t = iter.item + " " + t;
                iter = iter.next;
            }
        }
	return "[ " + t + ")";
    }
    public static void main(String[] args) { // Test-Unit
	Stack<String> stack = new EVLStack<String>(); // 
	String s;
	if (args.length > 0) System.out.println(stack);
	while (!StdIn.isEmpty()) {
	    s = StdIn.readString();
	    stack.push( s);
	    if (args.length > 0) System.out.println(stack);
	}
	while (!stack.empty() ) { 
	    s = stack.pop();  
	    System.out.println(s);
	    if (args.length > 0) System.out.println(stack);
	}
    }
}



    

