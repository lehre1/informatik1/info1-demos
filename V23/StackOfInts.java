public interface StackOfInts { 
    // kein Konstruktor
    public boolean empty();  // true, falls nichts gespeichert ist, sonst false
    public void push(int d); // speichert d ein
    public int pop();        // liefert zuletzt gespeicherten Wert und loescht ihn
    // fuer bequemeres Arbeiten zusaetzlich diese Schnittstelle:
    public int peek();       // liefert zuletzt gespeicherten Wert (ohne Loeschen)
}







    

