public interface Queue<Item> {
    public boolean empty();
    public void enqueue(Item item);
    public Item dequeue(); 
}
