public class TOHiterativ  {
  public static void main(String[] args) {
	// Tuerme initialisieren
    StackOfInts[] ts = {new ArrayStackOfInts(),new ArrayStackOfInts(),new ArrayStackOfInts()};
    int n = Integer.parseInt(args[0]);
    for (int i = n; i > 0; i--) ts[0].push(i); // Scheiben aufstapeln
    tprint(ts); // Anfangssituation auf stdout ausgeben
	
    int bew // Bewegungsrichtung Scheibe 1: je nach "Paritaet" von n
        = 1+(n%2); // .. 0 -> 1 -> 2 -> 0 -> 1 -> ... (falls n gerade)
                   // .. 0 -> 2 -> 1 -> 0 -> 2 -> ... (falls n ungerade) 
        // in jedem Fall: von 'pos' nach '(pos+bew)%3' 
    int pos = 0; // aktuelle Position von Scheibe 1
    for (int i = 1; i < (int) Math.pow(2,n); i++) { // 2^n-1 Schritte
      if (i%2 == 1) { // in ungeraden Schritten wird Scheibe 1 gezogen ..
        ts[(pos+bew) % 3].push(ts[pos].pop()); pos = (pos+bew) % 3; 
      } // .. in geraden Schritten der einzige andere legale Zug gemacht:
      else legalMove(ts[(pos+1) % 3], ts[(pos+2) % 3]);
      tprint(ts); // aktuelle Zustaende der Tuerme auf stdout ausgeben
    }
  }
  private static void legalMove(StackOfInts s, StackOfInts t) {
    int a = 0, b = 0;
    if (!s.empty()) a = s.peek(); // a bzw. b sind positiv genau dann, ..
    if (!t.empty()) b = t.peek(); // .. wenn s bzw. t nicht leer. 
    if (a < b) 
        if (a > 0) t.push(s.pop()); else s.push(t.pop()); 
    if (b < a)
        if (b > 0) s.push(t.pop()); else t.push(s.pop()); 
  }
  private static void tprint(StackOfInts[] ts) {
    for (int i = 0; i<ts.length; i++) System.out.println(ts[i]);
    System.out.println("----------------------");	
  }
} 
