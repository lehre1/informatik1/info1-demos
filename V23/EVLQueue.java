public class EVLQueue<Item> implements Queue<Item> {
    private Node first;
    private Node last; 
    private class Node {             
        Item item; Node next;        
        Node(Item it, Node ne) {item = it; ne = next;} 
    }
    public boolean empty() {return (first == null); }     
    public void enqueue(Item item) { // stellt item ans Ende der Liste
        Node oldlast = last;
        last = new Node(item, null);
        if (this.empty()) first = last;
        else oldlast.next = last;
    }
    public Item dequeue() {
        if (this.empty()) throw new RuntimeException("dequeue von leerer EVLQueue!"); 
        Item item = first.item; first = first.next;
        if (this.empty()) last = null;
        return item;
    }
    public String toString() {
	String t = "";
	Node cursor = first;
	String seperator;
	while (cursor != null) {
	    if   (cursor == last) seperator = " ";
	    else                  seperator = ",";
	    t += (cursor.item + seperator);
	    cursor = cursor.next;
	}
	return "< " + t + "<";
    }
    public static void main(String[] args) { // Test-Unit
	Queue<String> q = new EVLQueue<String>(); 
	String str;
	if (args.length > 0) System.err.println(q);
	while (!StdIn.isEmpty()) {
	    str = new String(StdIn.readLine());
	    q.enqueue( str);
	    if (args.length > 0) System.err.println(q);
	}
	while (!q.empty() ) { // s.first() liefert Object-Referenz ..
	    str = q.dequeue();  // .. => cast auf String erforderlich
	    System.out.println(str);
	    if (args.length > 0) System.err.println(q);
	}
    }
}
