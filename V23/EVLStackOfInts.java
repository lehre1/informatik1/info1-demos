public class EVLStackOfInts implements StackOfInts { 
    private Node first;
    private class Node {  // Eine *INNERE* Klasse(!). Das erspart separaten ..
        int i; Node next; // .. Quelltext Node.java - sonst i.W. gleichwertig.
        Node(int i, Node next) { this.i = i; this.next = next; }
    }
    public boolean empty() { return ( first == null); }
    public void push(int i) {
        Node oldfirst = first;
        first = new Node(i,oldfirst);
    }
    public int pop() {
	if ( empty()) throw new RuntimeException("pop von leerem Stack!");
	int i = first.i; first = first.next;
        return i;
    }
    public int peek() {
	if ( empty()) throw new RuntimeException("peek von leerem Stack!");
        return first.i;
    }
    public String toString() {
	String t = "";
        if (first != null) {
            Node iter = first;
            while( iter != null) {
                t = iter.i + " " + t;
                iter = iter.next;
            }
        }
	return "[ " + t + ")";
    }
    public static void main(String[] args) { // Test-Unit
	StackOfInts s = new EVLStackOfInts(); // Statischer Typ: Stack, dynamischer Typ: EVLStack
	int i;
	if (args.length > 0) System.err.println(s);
	while (!StdIn.isEmpty()) {
	    i = StdIn.readInt();
	    s.push( i);
	    if (args.length > 0) System.err.println(s);
	}
	while (!s.empty() ) { 
	    i = s.pop();  
	    System.out.println(i);
	    if (args.length > 0) System.err.println(s);
	}
    }
}

