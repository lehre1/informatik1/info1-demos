public class ArrayStackOfInts implements StackOfInts { // als Feld
    final private  int MAX = 16; // Anfangskapazitaet
    private  int[] elements;
    private  int count;          // Anzahl gespeicherter Werte
    public ArrayStackOfInts() { elements = new int[MAX];	count = 0;}
    public boolean empty() { return ( count == 0); }    
    private  void resize(int len) { // dynamische Laengenanpassung
	int[] tmp = new int[len]; // durch ..
	for (int i = 0; i < count; i++)  // .. Umspeicherung ..
	    tmp[i] = elements[i];        // .. in neues Feld und ..
	elements = tmp;}                 // .. Bindung an Instanzvariable
    public void push(int i) {
	if ( count == elements.length)   // Kapazitaet erreicht
	    resize( 2 * elements.length);
	elements[count] = i;
        count++;
    }
    public int pop() {
	if ( empty()) throw new RuntimeException("pop von leerem Stack!");
	int i = elements[count-1];
	count--;
	if ( MAX < count && count <= elements.length/4)
	   resize(elements.length/2);
	return i;
    }
    public int peek() { // wie pop(), nur ohne   count--   ...
	if ( empty()) throw new RuntimeException("peek von leerem Stack!");
	int i = elements[count-1];
        return i;
    }
    public String toString() {
	String t = "";
	for ( int i = 0; i < count; i++)
	    t = t + elements[i] + " ";
	return "[ " + t + ")";
    }
    public static void main(String[] args) { // Test-Unit
	StackOfInts s = new ArrayStackOfInts(); // Statischer Typ: Stack, dynamischer Typ: ArrayStack
	int i;
	if (args.length > 0) System.out.println(s);
	while (!StdIn.isEmpty()) {
	    i = StdIn.readInt();
	    s.push( i);
	    if (args.length > 0) System.out.println(s);
	}
	while (!s.empty() ) { 
	    i = s.pop();  
	    System.out.println(i);
	    if (args.length > 0) System.out.println(s);
	}
    }
}



    

