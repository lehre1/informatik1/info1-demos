public interface Stack<Item> { 
    public boolean empty();
    public void push(Item d);
    public Item pop();
    public Item peek();
}





    

