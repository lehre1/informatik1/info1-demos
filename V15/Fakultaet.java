public class Fakultaet {
    public static void main(String[] args) {
	final int N = Integer.parseInt(args[0]);
	for (int n = 1; n <= N; n++)
	    System.out.println(n + " " + factorial(n));
    }
    public static long factorial(int n) {
	// if (n <= 1) return n; // Basisfall
	return n * factorial(n - 1);
    }
}
