// http://introcs.cs.princeton.edu/java/41analysis/DoublingTest.java
/******************************************************************************
 *  Compilation:  javac DoublingTest.java
 *  Execution:    java DoublingTest
 *
 *  % java DoublingTest 
 *      512 6.48
 *     1024 8.30
 *     2048 7.75
 *     4096 8.00
 *     8192 8.05
 *   ... 
 *
 ******************************************************************************/

public class DoublingTest {
    public static double timeTrial(int N) {
        int[] a = new int[N];
        for (int i = 0; i < N; i++) {
            a[i] = StdRandom.uniform(2000000) - 1000000;
        }
        Stopwatch s = new Stopwatch();
        int cnt = ThreeSum.count(a);
        return s.elapsedTime();
    }
    // in main: Aufruf mit N=256, 512, 1024, ... und Tabelle ausgeben
    public static void main(String[] args) { 
	// formatierte Ausgabe (siehe Abschnitt 3.1.3)
        System.out.printf("%7s %7s %4s\n", "N", "Zeit", "Faktor");
        double prev = timeTrial(256);
        for (int N = 512; N < 20000; N += N) {
            double curr = timeTrial(N);
            System.out.printf("%7d %7.2f %4.2f\n", N, curr, curr / prev);
            prev = curr;
        } 
    } 
} 
