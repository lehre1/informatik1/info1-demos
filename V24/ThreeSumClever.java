public class ThreeSumClever {
    // siehe https://en.wikipedia.org/wiki/3SUM
    public static int count(int[] a) { 
        final int N = a.length;
        java.util.Arrays.sort(a);        
        int x, y, z, lo, hi, cnt = 0;                   
        for (int i = 0; i < N-2; i++) {
            x = a[i]; lo = i+1; hi = N-1;
            while (lo < hi) {
                y = a[lo]; z = a[hi];
                if (x + y + z == 0) { cnt++;
                    if (y == a[lo+1]) lo++;
                    else hi--;
                }
                else if (x+y+z>0) hi--;
                else lo++;
            }
        }
        return cnt;
    } 
    // print distinct triples (i, j, k) such that a[i] + a[j] + a[k] = 0
    public static void printAll(int[] a) {
	// ...
        int N = a.length;
        for (int i = 0; i < N; i++) 
            for (int j = i+1; j < N; j++) 
                for (int k = j+1; k < N; k++) 
                    if (a[i] + a[j] + a[k] == 0) 
                        System.out.println(a[i] + " " + a[j] + " " + a[k]);
    } 
    // ...
    public static void main(String[] args)  { 
	// ... liest Werte von StdIn in Array, ruft count(..), berichtet
        int[] a = StdArrayIO.readInt1D(); 
        // int N = StdIn.readInt(); 
        // int[] a = new int[N]; 
        // for (int i = 0; i < N; i++) a[i] = StdIn.readInt();
	Stopwatch timer = new Stopwatch();
        int cnt = count(a);
	if (args.length > 0) 
	    System.out.println("elapsed time = " + timer.elapsedTime());
        System.out.println(cnt);
	/* 
	if (cnt < 10)
            printAll(a);
	*/
    } 
} 
