public class SortedQueue<Item extends Comparable<Item>> {
    private Node first, last; 
    private class Node { 
	Item item;	Node next;
	Node (Item d, Node n) { item = d; next = n;}
    }
    // ...
    public boolean empty() {return (first == null); }     
    public void insert( Item s) {
	Node neu = new Node( s, null);
	Node a = null, b = first;
	while (b != null && neu.item.compareTo( b.item) > 0) {
	    a = b; b = b.next;
	}
	if ( a == null) first = neu;
	else a.next = neu;
	neu.next = b;
    }
    public Item dequeue() {
        if (this.empty()) throw new RuntimeException("dequeue von leerer EVLQueue!"); 
        Item item = first.item; first = first.next;
        if (this.empty()) last = null;
        return item;
    }
    public String toString() {
	String t = "";
	Node cursor = first;
	String seperator;
	while (cursor != null) {
	    if   (cursor == last) seperator = " ";
	    else                  seperator = ",";
	    t += (cursor.item + seperator);
	    cursor = cursor.next;
	}
	return "< " + t + "<";
    }
    public static void main(String[] args) { // Testunit
        SortedQueue<String> q = new SortedQueue<String>();
        String s;
        while (!StdIn.isEmpty()) {
            s = StdIn.readString();
            q.insert(s);
            if (args.length > 0) System.out.println(q);
        }
        while (!q.empty()) {
            System.out.println(q.dequeue());
            if (args.length > 0) System.out.println(q);
        }        
    }
}
