/*
siehe auch 
https://introcs.cs.princeton.edu/java/31datatype/Scale.java.html
*/

import java.awt.Color;

public class PictureTools {
    public static Picture scale(Picture source, int w, int h) {
        Picture target = new Picture( w, h);
        for (int ti = 0; ti < w; ti++) {     // Fuer jeden Pixel in ..
            for (int tj = 0; tj < h; tj++) {   // .. jeder Zeile des Zielbilds ..
                int si = ti * source.width()/w;  // .. ermittle jeweiligen Quellpixel ..
                int sj = tj * source.height()/h; // .. durch *Dreisatzrechnung*. 
                // Setze nun Zielpixel-Farbe auf Farbe des Quellpixels:
                target.set(ti,tj,source.get(si,sj));
            }
        }
        return target;
    }
    public static void main(String[] args) {
    String filename = args[0];
    int w=Integer.parseInt(args[1]), h=Integer.parseInt(args[2]);
        Picture source=new Picture(filename), target=scale(source, w, h);
    source.show();
    target.show();
    Picture stretched=scale(target, source.width(), source.height());
    stretched.show();
  }
}
