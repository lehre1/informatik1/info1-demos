public class Charge {
    private final double rx, ry;   // Position
    private final double q;        // Ladung

    public Charge(double x0, double y0, double q0) {
        rx = x0; ry = y0; q  = q0;
    }

    public double potentialAt(double x, double y) {
        double k = 8.99e09;
        double dx = x - rx, dy = y - ry;
        return k * q / Math.sqrt(dx*dx + dy*dy);
    }
    public String toString() {
        return q + " at (" + rx + ", " + ry + ")";
    }

    public static void main(String[] args) {
        double x = Double.parseDouble(args[0]), y = Double.parseDouble(args[1]);
        Charge c1 = new Charge(0.51, 0.63, 21.3), c2 = new Charge(0.13, 0.94, 81.9);
        System.out.println(c1); System.out.println(c2);
        double v = c1.potentialAt(x, y) + c2.potentialAt(x, y);
        System.out.println(v);
    }
}
