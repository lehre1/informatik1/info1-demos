/******************************************************************************
 *  Compilation:  javac Potential.java
 *  Execution:    java Potential < input.txt
 *  Dependencies: Charge.java Picture.java StdIn.java
 *                https://introcs.cs.princeton.edu/java/31datatype/charges.txt
 *
 *  Potential value visualization for a set of charges.
 *
 *  % java Potential < charges.txt
 *
 *
 ******************************************************************************/

import java.awt.Color;

public class Potential {
  public static void main(String[] args) {
    int n = StdIn.readInt();
    Charge[] a = new Charge[n];
    for (int k = 0; k < n; k++) 
      a[k] = new Charge(StdIn.readDouble(),StdIn.readDouble(),StdIn.readDouble());

    int SIZE = 800;
    Picture pic = new Picture(SIZE, SIZE);
    for (int i = 0; i < SIZE; i++) {
      for (int j = 0; j < SIZE; j++) {
        double potential = 0.0; // Akkumulator
        for (int k = 0; k < n; k++) {
          double x = 1.0 * i / SIZE, y = 1.0 * j / SIZE;
          potential += a[k].potentialAt(x, y);
        }
        // Diskretisierung, um Graustufe zuzuordnen
        int t = 128 + (int) (potential / 2.0e10);
        if      (t < 0) t = 0;
        else if (t > 255) t = 255;
        
        Color c = new Color(t, t, t);
	// andere Variante:
        int gray = (t * 15) % 256; 
        c = new Color(gray, gray, gray); // mit Aequipotentialen
        pic.set(i, SIZE-1-j, c);
      }
    }
    pic.show();
    pic.save("charges.jpg");
  }
}
