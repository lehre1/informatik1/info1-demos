import java.awt.Color;
public class QRCode {
    // liefere Anzahl der "Module" (= Pixel-Zeilen) für 2D-Code je nach Version 
    public static int modules(int version) {
        // https://de.wikipedia.org/wiki/QR-Code#Versionsgr%C3%B6%C3%9Fen
        return (version-1)*4+21;
    }
    public static Picture bitonic(Picture pic, int threshold) {
        Picture bit = new Picture(pic.width(),pic.height());
        for (int col = 0; col < pic.width(); col++) {
            for (int row = 0; row < pic.height(); row++) {
                Color color = pic.get(col, row);
                Color bitone = Luminance.intensity(color) < threshold ?
                    Color.BLACK : Color.WHITE;
                bit.set(col, row, bitone);
            }
        }
        return bit;
    }
    public static int[][] bitArray (Picture pic, int threshold) {
        int w = pic.width(), h = pic.height();
        int[][] bita = new int[h][w];
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                Color color = pic.get(j, i);
                bita[i][j] = 
                    Luminance.intensity(color)<threshold ? 0 : 1;
            }
        }
        return bita;
    }    
    public static void main(String[] args) {
        Picture pic = new Picture(args[0]);
        int w = pic.width(), h = pic.height();
        int m = 21;
        if (args.length>1) {
            int version = Integer.parseInt(args[1]);
            m = modules(version);            
        }
        int th = 127;
        Picture bit = bitonic(PictureTools.scale(pic,m,m),th);
        PictureTools.scale(bit,(int)(10.*w/m),(int)(10.*w/m)).show();

        int[][] bita = bitArray(bit,th);
        StdArrayIO.print(bita);
    }
}
