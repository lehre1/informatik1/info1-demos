public class Insertion {
    public static void sort(double[] f) { // Insertionsort
      int N = f.length; 
      for (int i = 1; i < N; i++) { // f[0], ..., f[i-1] ist bereits sortiert
	// f[i] an richtiger Stelle einfuegen, d.h soweit noetig ..
	  for (int j = i; j > 0 && f[j] < f[j-1]; j--) { // f[i] nach links ..
              double tmp=f[j]; f[j]=f[j-1]; f[j-1]=tmp; // .. "durchtauschen".
	  }
      }
    }  
    private static double timeTrial(int n) {
        double[] a = new double[n];
        for (int i = 0; i < n; i++) {
            a[i] = StdRandom.uniform(2000000) - 1000000;
        }
        Stopwatch s = new Stopwatch();
        sort(a);
        return s.elapsedTime();
    }
    public static void main(String[] args) {
	// ... }
	if ( args.length > 0) {
	    int n = Integer.parseInt(args[0]); 
	    double[] a = new double[n];
	    for (int i = 0; i < n; i++)
		a[i] = StdIn.readDouble();
	    sort(a);
	    for (int i = 0; i < n; i++)
		System.out.println(a[i]);
	}
        else { // Doublingtest
	    System.out.printf("%7s %7s %4s\n", "size", "time", "ratio");
	    double prev = timeTrial(256);
	    int K = 6;
	    int upper = (int) Math.pow(10,K);
	    for (int N = 512; N < upper; N += N) {
		double curr = timeTrial(N);
		System.out.printf("%7d %7.2f %4.2f\n", N, curr, curr / prev);
		prev = curr;
	    } 
	} 	
    }
}
