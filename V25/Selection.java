public class Selection {
    public static void sort(double[] f) { // Selectionsort
	int N = f.length; 
	for (int i = 0; i < N-1; i++) {
	    int m = i;
	    for (int j = i+1; j < N; j++) // Index m  des Minimums von ..
		if ( f[j] < f[m])         // .. [f[i],f[i+1],...,f[N-1]] ..
		    m = j;                // .. bestimmen und nach Position ..
	    double tmp=f[m]; f[m]=f[i]; f[i]=tmp; // .. i tauschen
	} 
    }
    private static double timeTrial(int n) {
        double[] a = new double[n];
        for (int i = 0; i < n; i++) {
            a[i] = StdRandom.uniform(2000000) - 1000000;
        }
        Stopwatch s = new Stopwatch();
        sort(a);
        return s.elapsedTime();
    }
    public static void main(String[] args) {
	if ( args.length > 0) {
	    int n = Integer.parseInt(args[0]); 
	    double[] a = new double[n];
	    for (int i = 0; i < n; i++)
		a[i] = StdIn.readDouble();
	    sort(a);
	    for (int i = 0; i < n; i++)
		System.out.println(a[i]);
	}
        else { // Doublingtest
	    System.out.printf("%7s %7s %4s\n", "size", "time", "ratio");
	    double prev = timeTrial(256);
	    int K = 6;
	    int upper = (int) Math.pow(10,K);
	    for (int N = 512; N < upper; N += N) {
		double curr = timeTrial(N);
		System.out.printf("%7d %7.2f %4.2f\n", N, curr, curr / prev);
		prev = curr;
	    } 
	} 	
    }
}
