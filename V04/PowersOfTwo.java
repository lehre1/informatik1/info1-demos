public class PowersOfTwo {
    public static void main (String[] args) {
	int n=Integer.parseInt(args[0]), p=1;
	int i;                          // <--
	for ( i=0; i<=n; i=i+1) {       // <--
	    System.out.println(i + " " + p);
	    p=2*p;
	}
    }
}

/*
// das Gleiche mit for-Schleife in Kurzform
public class PowersOfTwo {
    public static void main (String[] args) {
	int n=Integer.parseInt(args[0]), p=1;
	for (int i=0; i<=n; i++) {     // <--
	    System.out.println(i + " " + p);
	    p=2*p;
	}
    }
}
*/
