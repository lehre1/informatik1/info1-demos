public class PlotGaussian { // Standardnormalverteilung visualisieren
    public static void main(String[] args) {
	final int N = Integer.parseInt(args[0]);
	double[] d = new double[N+1], v = new double[N+1];
	for (int i = 0; i <= N; i++) {
	    d[i] = Gaussian.phi(-4.0 + 8.0 * i/N);
	    v[i] = Gaussian.Phi(-4.0 + 8.0 * i/N);
	}
	StdDraw.setYscale(0,1);
	StdDraw.setPenRadius(0.01);
	StdDraw.setPenColor(StdDraw.RED);
	StdStats.plotBars(d);  StdStats.plotLines(d);
	StdDraw.setPenColor(StdDraw.BLUE);
	StdStats.plotPoints(v); StdStats.plotLines(v);
    }
}
