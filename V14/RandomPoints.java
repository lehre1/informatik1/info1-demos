public class RandomPoints {
    public static void main(String[] args) {
	int N = Integer.parseInt(args[0]);
	double mean = .5;
	double stdv = .1;
	if (args.length == 3) {
	    mean = Double.parseDouble(args[1]);
	    stdv = Double.parseDouble(args[2]);
	}
        StdDraw.setPenRadius(0.005);
	for (int i = 0; i < N; i++) {
	    double x = StdRandom.gaussian(mean,stdv);
	    double y = StdRandom.gaussian(mean,stdv);
	    StdDraw.point(x,y);
	}
    }
}
