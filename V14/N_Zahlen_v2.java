public class N_Zahlen_v2 {
    public static void main(String[] args) {
        int N = Integer.parseInt(args[0]);
        int range = Integer.parseInt(args[1]);
        // Gib Werte aus im StdArrayIO-Eingabeformat:
        System.out.println(N);
        for (int i = 0; i < N; i++) {
            System.out.print((int) (Math.random()*range) + "\t");
        }
        System.out.println();
    }
}
