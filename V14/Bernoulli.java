// http://introcs.cs.princeton.edu/java/22library/Bernoulli.java.html
// leicht veraendert: 
// - Achsenskalierung angepasst
// - Kommentare ins Deutsche uebersetzt
/******************************************************************************
 *  Compilation:  javac Bernoulli.java
 *  Ausfuehrung:    java Bernoulli N T
 *  Abhaengigkeiten: StdDraw.java StdRandom.java Gaussian.java StdStats.java
 *  
 *  Jedes Experiment besteht daraus, n Muenzen zu werfen.
 *  Es werden t Experimente gemacht und ein Histogramm geplottet, 
 *  wieviele davon genau i mal Kopf (und n-i mal Zahl) lieferten.
 *  [Die theoretische Schätzung für diese Anzahl ist t*phi(i/n) 
 *  - die Genauigkeit steigt mit n.]
 *
 *  % java Bernoulli 32 1000
 *
 *  % java Bernoulli 64 1000
 *
 *  % java Bernoulli 128 1000
 *
 ******************************************************************************/



public class Bernoulli { 
    // Anzahl der 'Kopf'-Resultate beim Werfen von n 
    // p-gewichteten Muenzen: Kopf mit W'keit p
    public static int binomial(int n, double p) {
        int heads = 0;
        for (int i = 0; i < n; i++) {
            if (StdRandom.bernoulli(p)) {
                heads++;
            }
        }
        return heads;
    } 
    // Anzahl der 'Kopf'-Resultate beim Werfen von n 
    // fairen Muenzen: Kopf mit W'keit 1/2
    public static int binomial(int n) {
        int heads = 0;
        for (int i = 0; i < n; i++) {
            if (StdRandom.bernoulli(0.5)) { // eine faire Muenze werfen
                heads++;
            }
        }
        return heads;
    } 
    public static void main(String[] args) { 
        int n = Integer.parseInt(args[0]);   // Anzahl der Wuerfe in jedem Versuch
        int t = Integer.parseInt(args[1]);   // Anzahl der Versuche

        StdDraw.setYscale(0, 1./Math.sqrt(n));
        StdDraw.setXscale(0, 1./Math.sqrt(t));
        // t mal 'n Muenzen werfen': zaehle wie oft welche Anzahlen vorkommen
        int[] freq = new int[n+1];
        for (int j = 0; j < t; j++) {
            freq[binomial(n,0.5)]++;
        }

        // Anteile plotten
        double[] normalized = new double[n+1];
        for (int i = 0; i <= n; i++) {
            normalized[i] = (double) freq[i] / t;
        }
        StdStats.plotBars(normalized);

        // (approximierte) Normalverteilung dazu plotten
        // ...
        double mean = n / 2.0;
        double stddev = Math.sqrt(n) / 2.0;
        double[] phi  = new double[n+1];
        for (int i = 0; i <= n; i++) {
            phi[i] = Gaussian.phi(i, mean, stddev);
        }
        StdStats.plotLines(phi);
    } 
}
