public class PlotSamples { // Messwerte visualisieren
    public static void main(String[] args) {
	double[] a = StdArrayIO.readDouble1D(); // Messwerte einlesen
	StdDraw.setYscale(StdStats.min(a),StdStats.max(a)); // Ordinate skalieren
	StdStats.plotPoints(a);	
    }
}
