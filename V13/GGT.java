public class GGT {
    public static int ggt(int x, int y) {
	int r;
	while ( y != 0) { 
	    r = x % y; 
	    x = y; 
	    y = r;
	}
	return x; 
    }    
    public static void main( String[] args){
        int n = args.length;
        int t = 0;
        if (n==0)
            return;
        for (int i=0; i<n; i++) {
            int a = Integer.parseInt(args[i]);
            t = ggt(t,a);
        }
	System.out.println(t); 
    } 
}
