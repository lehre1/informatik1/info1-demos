public class RandomInt { 
    public static void main(String[] args) { 
        int N = Integer.parseInt(args[0]);
        double r = Math.random();//Zufallswert im Intervall [0,1)
        int n = (int) (r * N);   //Zufallswert aus {0, 1, ..., N-1}
        System.out.println(n);
    }
}
