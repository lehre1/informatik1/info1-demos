/* nach Sedgewick/Wayne, S. 104ff.
   Aufruf mittels 'java Exponential <zahl>' (sonst Prozessabbruch) */ 

public class Exponential {
    public static void main(String[] args) {
	double x = Double.parseDouble(args[0]);
	// 1. Idee: summiere "viele" (z.B. 100) Terme der Form x^i/i!
	/* Berechnung der Terme:
	double nenner = 1., zaehler = 1.; 
	for (int i = 1; i <= n; i++) zaehler = zaehler * x;
	for (int i = 1; i <= n; i++) nenner = nenner * i;
	double term = zaehler/nenner;
	// Nachteile:
	// - zwei for-Schleifen zu umstaendlich
	// - bei grossem n Ueberlauf in zweiter Schleife 
	*/ 
	/* effizientere Berechnung der Terme: 
	double term = 1.; 
       	for (int i = 1; i <= n; i++) term = term * x/i;
	*/
	/* Einbettung in for-Schleife zum Aufsummieren 
	double term = 1.;
	double sum = 0.;                 // Akkumulator
	for (int n = 1; sum != sum + term; n++){//Stopp bei Unterlauf
		sum = sum + term;
		term = 1.;
		for (int i = 1; i <= n; i++) term = term * x/i;
	    }
	// Nachteil: Z.B. bei n==2 werden in innerer Schleife 
	// x/1 und x^2/2 berechnet. Bei n==3 werden sie wieder gebraucht 
	// und nochmal berechnet
	*/
	double term = 1.;
	double sum = 0.;               // Akkumulator
	for (int n = 1; sum != sum + term; n++){//Stopp bei Unterlauf 
		sum = sum + term;
		term = term * (x/n);
	    }
	System.out.println(sum);
    }
}
