public class Sqrt {
    public static void main( String[] args) {
        double a = Double.parseDouble(args[0]);
	double epsilon = 1e-15;
	double r = a;
        int i = 0;
	while (Math.abs( r-a/r) > epsilon * r) {
            i++;
            r = (r + a/r)/2.0; // r <- Mittelwert von r und a/r
            System.out.println(i+": "+r);
	}
	System.out.println(r);
    }
}
