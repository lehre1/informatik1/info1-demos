public class HelloFloats {
    public static void main(String[] args) {
        {
            float sum = 0.0f;
            float a = 0.1f;
            for (int i = 0; i < 10; i++) {
                System.out.println(i + " x 0.1 = " + Float.toHexString(sum));
                sum += a;
            }
            System.out.println("10 x 0.1 = " + sum);
        }
        // {
        //     double sum = 0.0f;
        //     double a = 0.1f;
        //     for (int i = 0; i < 10; i++) {
        //         System.out.println(i + " x 0.1 = " + Double.toHexString(sum));
        //         sum += a;
        //     }
        //     System.out.println("10 x 0.1 = " + sum);
        // }
    }
}
