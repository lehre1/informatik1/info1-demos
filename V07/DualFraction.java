public class DualFraction {
    public static void main(String[] args) {
	double z;
	do { 
	    z = StdIn.readDouble(); 
	} while (z < 0 || z >= 1); 
        // Jetzt gilt: 0 <= z < 1.
  
	System.out.print("0.");
        do {z *= 2;
	    if (z >= 1) { 
	       System.out.print(1); 
               z -= 1; 
	    } 
	    else 
	       System.out.print(0); 
	} while (z > 0);
	System.out.println();
    }
}
