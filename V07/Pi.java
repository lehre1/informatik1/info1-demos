public class Pi {
    public static void main  (String[] args) {
	int a = 0;     
	int r  = Integer.parseInt(args[0]);
	for (int y = 1; y <= r; y++) 
	    for (int x = 0; x <= r; x++) 
		if (y*y + x*x <= r*r) 
		    a++;
	System.out.println( 4.0 * a/((r+1)*(r+1)));
    }
}

