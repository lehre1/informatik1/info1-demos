public class Schaltjahr {
    public static void main(String[] args) {
	int j = Integer.parseInt(args[0]);
	if ( ((j % 4 == 0) &&     // durch 4 teilbar
             (j % 100 != 0))      // nicht durch 100
             || (j % 400 == 0))  // oder aber durch 400
            System.out.println(j + " ist ein Schaltjahr.");
	else
            System.out.println(j + " ist kein Schaltjahr.");
    }
}
