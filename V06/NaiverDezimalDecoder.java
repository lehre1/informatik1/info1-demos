public class NaiverDezimalDecoder {
    public static void main(String[] args) {
        int N=Integer.parseInt(args[0]);
        int z=0;
        char c;
        for (int i=0; i<N; i++) {// N mal:
                                 // (1) eine Ziffer lesen: 
            do {
                c = StdIn.readChar(); // (dazu die .. 
            } while (c<'0' || c>'9'); // Nicht-Ziffern ignorieren) 

            int w=0;             // (2) ihren Ziffernwert bestimmen:
            if (c=='1')      w=1;  
            else if (c=='2') w=2;
            else if (c=='3') w=3;
            else if (c=='4') w=4;
            else if (c=='5') w=5;
            else if (c=='6') w=6;
            else if (c=='7') w=7;
            else if (c=='8') w=8;
            else if (c=='9') w=9;
            for (int j=N-1-i;j>0;j--) // (3) und ihren *Stellen*wert 
                w = 10*w; 
            z = z+w;              // und (4) akkumulieren
        }
	System.out.println(z);
    }
}
