public class Ordne {
    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]),
            b = Integer.parseInt(args[1]),
            c = Integer.parseInt(args[2]);
	if (a<=b && b<=c) System.out.println(a+" "+b+" "+c);
        else if (a<=c && c<=b) System.out.println(a+" "+c+" "+b);
        else if (b<=a && a<=c) System.out.println(b+" "+a+" "+c);
        else if (b<=c && c<=a) System.out.println(b+" "+c+" "+a);
        else if (c<=a && a<=b) System.out.println(c+" "+a+" "+b);
        else if (c<=b && b<=a) System.out.println(c+" "+b+" "+a);
    }
}
