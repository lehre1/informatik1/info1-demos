public class DualCode {
    public static void main(String[] args) {
	int z = StdIn.readInt();
	String s = ""; // Akkumulator
	do { s = (z % 2) + s;
	    z /= 2;
	} while ( z != 0);
	System.out.println(s);
    }
}
