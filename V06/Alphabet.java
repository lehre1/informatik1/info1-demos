public class Alphabet {
    public static void main(String[] args) {
        int i = 0;
	for (char c = 0; c <= 12800; c++) {
            i++;
            if (!Character.isISOControl(c))
                System.out.print(c);
            if (i%16 == 0)
                System.out.println();
        }
    }
}
