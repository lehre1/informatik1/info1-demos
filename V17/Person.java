// Quelle: Skript "Algorithmen" (Prof. Vornberger, Osnabrueck)

import java.util.GregorianCalendar;
public class Person {
    private String vorname, nachname;
    private Datum geb_datum; 
    public Person (String vorname, String nachname, 
		   int tag, int monat, int jahr) { // Geburtsdatum
	this.vorname = vorname; this.nachname = nachname;
	geb_datum = new Datum(tag,monat,jahr); 
    }
    // anwendbare Methoden
    /*
      ...
    */
    public int jahrgang () {    // Methode
	return geb_datum.jahr;  // liefert Geburtsjahrgang
    }
    public int alter(){ // Methode
	int jetzt = new GregorianCalendar().get(GregorianCalendar.YEAR);
	return jetzt - geb_datum.jahr; // liefert das Lebensalter
    }
    public String toString() { // Methode
	return vorname + " " + nachname + " (" + geb_datum + ")"; 
    }
    
    /*
      ...
    */

    public static void main(String[] args) { // Test-Unit
	Person p = new Person("Georg Christoph", "Lichtenberg", 1, 7, 1742);
	System.out.println(p);
	System.out.println(p.geb_datum.jahr); // <---
        System.out.println(p.alter()); // <---
    }
}

