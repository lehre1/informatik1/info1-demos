// Nach: Vornberger (Osnabrueck), Skript "Algorithmen" 
public class Datum {
    /*
      ...
    */
    int tag; int monat; int jahr; 
    public Datum (int tag, int monat, int jahr){ // Konstruktor mit 3 Parametern
	this.tag = tag; this.monat = monat; this.jahr = jahr; 
    }
    public Datum (int jahr){ // Konstruktor mit einem Parameter
	this.tag = 1; this.monat = 1; this.jahr = jahr;
    }
    // Alternative: 
    /*
    public Datum (int j){ 
	this(1, 1, j); // initialisiere 1.1. Jahr   
    }
    */
    public Datum () { // parameterloser Konstruktor
	this( 1, 1, 1);
	// tag = 1; monat = 1; jahr = 1; // ist auch moeglich
    }
    public String toString(){ // Methode ohne Parameter
	return tag + "." + monat + "." + jahr; // liefert Datum als String
    }
    /*
      ...
    */
}
