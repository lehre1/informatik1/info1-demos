// Quelle: Skript "Algorithmen" (Prof. Vornberger, Osnabrueck)

public class Student { // Student erbt direkt von Person
    public String name;
    public int matrikelnr;                  // Matrikelnr 
    public String fach;                 // Studienfach
    public Student(String name, int matrikelnr, String fach) {  
        this.name = name;
	this.fach = fach;        // initialisiere nun noch ..
	this.matrikelnr = matrikelnr;
    }
}
