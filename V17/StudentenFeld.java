public class StudentenFeld {
    public static void main(String[] args) {
        int N = Integer.parseInt(args[0]);
        Student[] teiln = new Student[N];
        for (int i = 0; i < N; i++) {
            teiln[i] = // Konstruktoraufruf mit einzulesenden Daten:
                new Student(StdIn.readString(),StdIn.readInt(),StdIn.readString());
        }
        sortiereNachMatrikelnr( teiln);
        for (int i = 0; i < N; i++) {
            Student t = teiln[i];
            System.out.println(t.name + " " + t.matrikelnr + " " + t.fach);
        }
    }
    public static void sortiereNachMatrikelnr(Student[] f) { // InsertionSort
        int N = f.length;
        for (int i = 1; i < N; i++) { // f[0], ..., f[i-1] ist bereits sortiert
            // f[i] richtig einfuegen, d.h f[i] soweit noetig nach links ..
            for (int j = i; j > 0 && f[j].matrikelnr < f[j-1].matrikelnr; j--) {
                Student tmp=f[j]; f[j]=f[j-1]; f[j-1]=tmp; // .. "durchtauschen".
            }
        }
    }
}
