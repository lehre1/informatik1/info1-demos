public class Kartenspiel {
    public static void main(String[] args) {
        int[] blatt = new int[32];
        for (int i = 0; i < 32; i++) {
            blatt[i] = i;
        }
        StdRandom.shuffle(blatt);
        String[] farbe = {"Kreuz", "Pik", "Herz", "Karo"};
        String[] rang = {"Ass", "König", "Dame", "Bube", "10", "9", "8", "7"};
        for (int i = 0; i < 32; i++) {
            System.out.println(farbe[blatt[i]/8]+" "+rang[blatt[i]%8]);
        }
    }
}
