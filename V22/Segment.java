/* Datentyp zum Modellieren von Strecken und Geraden in der Ebene */ 
import java.awt.Color;
public class Segment{
    private final double ax, ay;        // Anfangspunkt-Koordinaten
    private final double len, ang;      // Laenge, Winkel zur Horizontalen

    public Segment(double x, double y, double len, double anteil) {
        ax = x; ay = y;
        this.len = len;
        this.ang = anteil*2*Math.PI;
    }
    public Segment(double x, double y, double len) {
        ax = x; ay = y; // Startpunkt
        this.len = len; // Länge
        this.ang = Math.random()*2*Math.PI; // zufälliger Winkel
    }    
    public Segment(double len) { 
        ax = Math.random(); ay = Math.random(); // zufälliger Startpunkt
        this.len = len;
        this.ang = Math.random()*2*Math.PI; // zufälliger Winkel
    }
    public Segment() { // alle Parameter zufaellig (analog)
        ax = Math.random(); ay = Math.random();
        this.len = Math.random();
        this.ang = Math.random()*2*Math.PI;
    }

    // Koordinaten des Endpunkts dieser Strecke
    private double endX() { return ax + len*Math.cos(ang); }
    private double endY() { return ay + len*Math.sin(ang); }

    private double intersectX (Segment othr){// x-Koordinate des 
        // Schnittpunkts dieser STRECKE mit der GERADEN othr
        double      bx = othr.ax, by = othr.ay,
            ex = this.endX(), ey = this.endY(), 
            fx = othr.endX(), fy = othr.endY();
        double sx = // siehe https://de.wikipedia.org/wiki/Schnittpunkt
            ((fx - bx)*(ex*ay - ax*ey) - (ex - ax)*(fx*by - bx*fy)) 
            / ((fy - by)*(ex - ax) - (ey - ay)*(fx - bx));
	double // liegt x-Projektion des GERADENschnittpunkts auf ..
	    mx = Math.min(ax,ex), Mx = Math.max(ax,ex); // .. x-Projektion ..
        if (mx <= sx && sx <= Mx) return sx;            // .. dieser Strecke?
	else return Double.NaN; // falls nicht: kein Schnittpunkt => Sonderwert
    }
    private double intersectY (Segment othr){// (analog fuer y)
        // Schnittpunkts dieser STRECKE mit der GERADEN othr
         double      bx = othr.ax, by = othr.ay,
            ex = this.endX(), ey = this.endY(), 
            fx = othr.endX(), fy = othr.endY(); 
        double sy = // siehe https://de.wikipedia.org/wiki/Schnittpunkt
            ((ay - ey)*(fx*by - bx*fy) - (by - fy)*(ex*ay - ax*ey)) 
            / ((fy - by)*(ex - ax) - (ey - ay)*(fx - bx));
	double // liegt y-Projektion des GERADENschnittpunkts auf ..
	    my = Math.min(ay,ey), My = Math.max(ay,ey); // .. y-Projektion ..
        if (my <= sy && sy <= My) return sy;            // .. dieser Strecke?
	else return Double.NaN; // falls nicht: kein Schnittpunkt => Sonderwert
    }    
    public boolean crosses(Segment othr) {
	double sx = this.intersectX(othr), sy = this.intersectY(othr);
        if (!Double.isNaN(sx) && !Double.isNaN(sy)) // direkter Vergleich ..
            return true;                     // .. (sx != Double.NaN) ..
	else return false;                   // .. ist NICHT MOEGLICH!
    }

    public void plot(Color c) {        
	StdDraw.setPenColor(c);
        double ax = this.ax, ay = this.ay;
        double ex = this.endX(), ey = this.endY(); 
        StdDraw.line(ax, ay, ex, ey);
    }
    
    public static void main(String[] args) {
	int N = 2; 
	if (args.length > 0) N = Integer.parseInt(args[0]);
	Segment[] l = new Segment[N];
	if (args.length == 0) {
	    System.out.println("Parameter fuer 2 Strecken eingeben: ");
	    l[0] = new Segment(StdIn.readDouble(),StdIn.readDouble(),
			    StdIn.readDouble(),2*StdIn.readDouble()*Math.PI);
	    l[1] = new Segment(StdIn.readDouble(),StdIn.readDouble(),
			    StdIn.readDouble(),2*StdIn.readDouble()*Math.PI);
	} else for (int i = 0; i < N; i++) 
                   l[i] = new Segment(); // Zufallsstrecke
        for (int i = 0; i < N; i++) l[i].plot(StdDraw.BLACK);
	if (args.length <= 1)
	    for (int i = 0; i < N-1; i++) 
		for (int j = i; j < N; j++) 
		    if (l[i].crosses(l[j])) {
			StdDraw.setPenColor(StdDraw.RED);
			StdDraw.setPenRadius(0.01);		    
			StdDraw.point(l[i].intersectX(l[j]),
				      l[i].intersectY(l[j]));
		    }
	if (args.length == 2)
	    System.out.println("Parameter fuer 1 Strecke eingeben: ");
	Segment lm = new Segment(StdIn.readDouble(),StdIn.readDouble(),
			   StdIn.readDouble(),2*StdIn.readDouble()*Math.PI);
	lm.plot(StdDraw.BLUE);
	for (int i = 0; i<N; i++) 
	    if (l[i].crosses(lm)) {
		StdDraw.setPenColor(StdDraw.RED);
		StdDraw.setPenRadius(0.01);		    
		StdDraw.point(l[i].intersectX(lm),
			      l[i].intersectY(lm));
	    }
    }
}
