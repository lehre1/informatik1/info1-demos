// Quelle: Skript "Algorithmen" (Prof. Vornberger, Osnabrueck)

public class Student extends Person { // Student erbt direkt von Person
    private static int next_mat_nr = 100000; // Klassenvariable
    private int mat_nr;                  // Matrikelnr 
    private String fach;                 // Studienfach
    private int jsb;                     // Jahr des Studienbeginns
    // Konstruktor
    public Student(String vn, String nn, // Name, Vorname
		   int t, int m, int j,  // Geburtstag (Tag, Monat, Jahr)
		   String f, int jsb) {  // Studienfach+Jahr d. Studienbeginns
	super(vn, nn, t, m, j);          // Konstruktor der Basisklasse
	fach = f; this.jsb = jsb;        // initialisiere nun noch ..
	mat_nr = next_mat_nr++;          // .. die anderen Instanzattribute.
    }
    // Zugriffsmethoden
    public int jahrgang() { return jsb;} // Jahr des Studienbeginns

    public int geburtsjahrgang() {       // Zugriff auf jahrgang()-..
        return super.jahrgang(); }       // .. Implementation von Person
}
