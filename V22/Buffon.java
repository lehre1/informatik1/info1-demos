import java.awt.Color;
public class Buffon {
    public static void main(String[] args) {
        if (args.length < 2) {
            StdOut.println("Aufruf: Buffon <Nadeln> <Linien>");
            return;
        }
        int N = Integer.parseInt(args[0]); // Anzahl der Nadeln
        int M = Integer.parseInt(args[1]);
        if (M<1) {
            StdOut.println("Mindestens 2 Linien!");
            return;
        }
        double d = 1./(M-1), ell = d; // Linienabstand und Nadellaenge
        Segment[] line = new Segment[M];
        for (int i = 0; i < M; i++) {
            line[i] = new Segment(0,i*d,1,0);
            line[i].plot(StdDraw.BLUE);
        }
        int c = 0; // "Crossings"-Zähler                    
        for (int i = 0; i < N; i++) {
            Segment needle = new Segment(ell); // Laenge fix, Rest zufaellig
            Color color = StdDraw.BLACK;
            for (int j = 0; j < M; j++) {      // wird eine ..
                if (needle.crosses(line[j])) { //.. Linie gekreuzt? ..
                    c++;                       //.. Dann zaehle mit.
                    color = StdDraw.RED;       
                    break;          // Keine weiteren Linien prüfen.
                }
            } 
            needle.plot(color);
        }
        System.out.println(c);
        System.out.println(2.*N/c);
    }
}
