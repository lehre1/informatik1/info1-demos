public class BinarySearch {
    public static void main(String[] args) {
	int N = Integer.parseInt(args[0]); // Länge
        int q = Integer.parseInt(args[1]); // Suchwert

        int[] f = new int[N]; 
        for (int i = 0; i < N; i++) {
            f[i] = StdIn.readInt();
        }
        int lo=0, hi=N;
	while (lo < hi) {           
            int mid=(hi + lo)/2, median=f[mid]; // mittl. Position u. Wert
            if      ( q < median) hi=  mid; 
            else if ( median < q) lo=mid+1; 
            else    { 
		System.out.println("Position: " + mid); 
		return; 
	    }
	}                       
	System.out.println("Nicht gefunden!");
    }
}
