public class Bubblesort {
    public static void main(String[] args) {
        int N = Integer.parseInt(args[0]);
	int[] f = new int[N];
	for (int i=0; i<N; i++) 
	    f[i] = StdIn.readInt();
        // sortieren:

	boolean swapped;// flag zeigt an: "Tausch in aktueller Iteration"
	do {swapped = false;              // -------------------------+                       
	    for (int i=0; i < N-1; i++)                          //   |
		if ( f[i]>f[i+1]) {                              //   |
		    int tmp = f[i]; f[i] = f[i+1]; f[i+1] = tmp; // <-+
		    swapped = true; // <------------------------------/
		}
	} while (swapped);
	// ... 
	for (int i=0; i<N; i++) 
	    System.out.println(f[i]);
    }
}
