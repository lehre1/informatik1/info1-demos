public class N_Wuerfel { 
    public static void main(String[] args) { 
        int N = Integer.parseInt(args[0]);
        double r;
        int[] anz = new int[6];
        for (int i = 0; i < N; i++) {
            r = Math.random();                 //Zufallswert im Intervall [0,1)
            int w = (int) (r * 6);
            anz[w]++;
        }
        for (int w = 0; w < 6; w++) {
            System.out.println(anz[w] + " mal die " + (w+1)); 
        }
    }
}
