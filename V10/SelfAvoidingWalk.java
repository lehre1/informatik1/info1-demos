// Nach http://introcs.cs.princeton.edu/java/24percolation/SelfAvoidingWalk.java
// siehe Sedgewick/Wayne S. 132
/******************************************************************************
 *  Compilation:  javac SelfAvoidingWalk.java
 *  Execution:    java SelfAvoidingWalk N T
 *
 *  Simulate T self-avoiding walks in an N x N-grid and count how many of them 
 *  reach dead ends before touching the border.
 *
 *  Output fraction of dead ending walks.
 *
 *  % java SelfAvoidingWalk 20
 *
 ******************************************************************************/

public class SelfAvoidingWalk { 
    public static void main(String[] args) {
        int N = Integer.parseInt(args[0]);
        int T = Integer.parseInt(args[1]);
	int deadEnds = 0; // dead end (engl. Sackgasse)
	for (int t=0; t<T; t++) {
            boolean[][] a = new boolean[N][N];
	    int x=N/2, y=N/2;             // Startposition
	    while ( x>0 && x<N-1 && y>0 && y<N-1) { 
		a[x][y] = true;                // falls ..
		if (a[x-1][y] && a[x+1][y]     // .. alle Nachbarn schon ..
		    && a[x][y-1] && a[x][y+1]) // .. besucht, dann Sackgasse ..
		    { deadEnds++; break; }     // .. gefunden, while sofort verlassen
		// sonst:
		double r = Math.random();      // zufaellig einen freien Nachbarn ..
		if      ( r<0.25 ) {if (!a[x+1][y]) x++;} // .. im Osten ..
		else if ( r<0.5  ) {if (!a[x-1][y]) x--;} // .. Westen ..
		else if ( r<0.75 ) {if (!a[x][y+1]) y++;} // .. Norden oder ..
		else if ( r<1    ) {if (!a[x][y-1]) y--;} // .. Sueden besuchen.
	    }
	}
	System.out.println(100.*deadEnds/T + "% Sackgassen");
    }
}
