import java.awt.Color;
public class RandomColor{
 public static void main(String[] args) {
     int r = (int) (Math.random()*256);
     int g = (int) (Math.random()*256);
     int b = (int) (Math.random()*256);
     Color mix = new Color(r,g,b);
     System.out.println(mix);
  StdDraw.setPenColor(mix);
  StdDraw.filledSquare(0.5,0.5,0.25);
 }
}
