import java.awt.Color;
public class Steelblue{
 public static void main(String[] args) {
  Color steelblue = new Color(70,130,180);
  StdDraw.setPenColor(steelblue);
  StdDraw.filledSquare(0.5,0.5,0.25);
 }
}
