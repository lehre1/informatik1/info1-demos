public class SomeDoubles {
    public static void main(String[] args) {
        int N = Integer.parseInt(args[0]);
        double range = Double.parseDouble(args[1]);
        for (int i = 0; i < N; i++) {
            double d = 2*range*Math.random()-range;
            System.out.println(d);
        }
    }
}
