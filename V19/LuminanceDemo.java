import java.awt.Color;
public class LuminanceDemo {
    // zeigt Zufallsfarbe, ihre Farbanteile und die Graustufe gleichzeitig in einer Grafik
    public static void main(String[] args) {
        int r = (int) (Math.random() * 256);
        int g = (int) (Math.random() * 256);
        int b = (int) (Math.random() * 256);
        Color c = new Color(r,g,b); // Zufallsfarbe
        System.out.println(c + ", Luminanz: " + (int) Luminance.intensity(c));
        
        StdDraw.setPenColor(c); // Stift in Zufallsfarbe c
        StdDraw.filledSquare(0.25,0.25,0.20);  // kleineres Quadrat links unten
        StdDraw.setPenColor(new Color(r,0,0)); // Stiftfarbe nach Rotfilter
        StdDraw.filledSquare(0.25,0.75,0.25);  // Quadrat links oben
        StdDraw.setPenColor(new Color(0,g,0)); // Stiftfarbe nach Grünfilter
        StdDraw.filledSquare(0.75,0.75,0.25);  // Quadrat rechts oben
        StdDraw.setPenColor(new Color(0,0,b)); // Stiftfarbe nach Blaufilter
        StdDraw.filledSquare(0.75,0.25,0.25);  // Quadrat rechts unten
        StdDraw.setPenColor(Luminance.toGray(c)); // Stiftfarbe = Graustufe
        StdDraw.filledSquare(0.5,0.5,0.25);    // Quadrat in der Mitte
    }
}
