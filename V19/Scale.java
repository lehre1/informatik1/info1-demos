/******************************************************************************
 *  Compilation:  javac-introcs Scale.java
 *  Execution:    java-introcs Scale filename w h
 *  Data files:   https://introcs.cs.princeton.edu/java/31datatype/mandrill.jpg
 *
 *  Scales an image to w-by-h and displays both the original
 *  and the scaled version to the screen.
 * 
 *  % java-introcs Scale mandrill.jpg 200 300
 *
 *
 ******************************************************************************/

import java.awt.Color;

public class Scale {
  public static void main(String[] args) {
    String filename = args[0];
    int w=Integer.parseInt(args[1]), h=Integer.parseInt(args[2]);
    Picture source=new Picture(filename), target=new Picture(w, h);

    for (int ti = 0; ti < w; ti++) {     // Fuer jeden Pixel in ..
      for (int tj = 0; tj < h; tj++) {   // .. jeder Zeile des Zielbilds ..
        int si = ti * source.width()/w;  // .. ermittle jeweiligen Quellpixel ..
        int sj = tj * source.height()/h; // .. durch *Dreisatzrechnung*. 
        // Setze nun Zielpixel-Farbe auf Farbe des Quellpixels:
        target.set(ti,tj,source.get(si,sj));
      }
    }
    source.show();
    target.show();
  }
}
