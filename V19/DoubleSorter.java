/* Eine Quelldatei kann mehrere Klassen enthalten, aber hoechstens eine public
   (deren Name dann genau dem Dateinamen entsprechen muss). */

public class DoubleSorter {
    private static ZahlKnoten anfang = null; 
    public static void insert( double zahl) { // sortiert einfuegen
	ZahlKnoten neu = new ZahlKnoten( zahl, null);
	ZahlKnoten a = null, b = anfang;
	while (b != null && b.zahl < neu.zahl ) {
	    a = b; b = b.next;
	}
	if ( a == null) anfang = neu; else a.next = neu;
	neu.next = b; }
    // .. main: jede eingelesene Zahl einfuegen, danach alle ausgeben ...
    public static void main(String[] args) {
	while (!StdIn.isEmpty()) {
	    double zahl = StdIn.readDouble();
	    insert( zahl); 
	}
	ZahlKnoten aktuell = anfang;
	while (aktuell != null) {
	    System.out.println(aktuell.zahl);
	    aktuell = aktuell.next;
	}
    }
}
class ZahlKnoten {
    double zahl;
    ZahlKnoten next; 
    public ZahlKnoten (double zahl, ZahlKnoten next) { 
	this.zahl = zahl; 
	this.next = next;
    }
}


