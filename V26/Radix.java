public class Radix {
    static int K = 10;  // Stellenzahl
    static int[] decimalPower = {
        1,
        10,
        100,
        1000,
        10000,
        100000,
        1000000,
        10000000,
        100000000,
        1000000000}; // Stellenwerte

    // digit(j,n) liefert j-te Dezimalziffer von n 
    private static int digit(int j, int n) {
        return (n / decimalPower[j]) % 10;
    }
    public static void sort(int[] f) {
	QueueOfInts[] q = new RingbufferOfInts[10];
	for (int d = 0; d < 10; d++) {
	    q[d] = new RingbufferOfInts();
	}
        int N = f.length;
        for (int j = K-1; j >= 0; j--) {
	    for (int i = 0; i < N; i++) 
		q[digit(j,f[i])].enqueue(f[i]);
	    int i = 0;
	    for (int d = 0; d < 10; d++)
		while (!q[d].empty()) {
		    f[i] = q[d].dequeue();
		    i++;
		}
	}
    }
    private static double timeTrial(int n) {
        int[] a = new int[n];
        int max = 1;
        for (int k = 0; k < K; k++)
            max *= 10;
        for (int i = 0; i < n; i++) {
            a[i] = StdRandom.uniform(max);
        }
        Stopwatch s = new Stopwatch();
        sort(a);
        return s.elapsedTime();
    }
    public static void main(String[] args) {
        if (args.length > 0) {
            int N = Integer.parseInt(args[0]); 
            int[] f = new int[N];
            int t;
            System.out.println(N + 
                               " nichtnegative ganze Zahlen eingeben:");
            for (int i = 0; i < N; i++) {
                do {
                    t = StdIn.readInt();
                    if ( t < 0) System.out.println("ignoriert, da negativ");
                } while (t < 0);
                f[i] = t;
            }
            sort(f);
            for (int i = 0; i < N; i++) {
                System.out.println(f[i]);
            }
            return;
        } 
        else { // Doublingtest
  	    System.out.printf("%7s %7s %4s\n", "size", "time", "ratio");
	    double prev = timeTrial(256);
	    K = 8;
	    int upper = (int) Math.pow(10,K);
	    for (int N = 512; N < upper; N += N) {
		double curr = timeTrial(N);
		System.out.printf("%7d %7.2f %4.2f\n", N, curr, curr / prev);
		prev = curr;
	    } 
        }
    }
}
