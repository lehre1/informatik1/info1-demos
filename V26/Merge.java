public class Merge {
    // sort rekursiv organisieren: 
    public static void sort(double[] f) {
	sort( f, 0, f.length);
    }
    
    public static void sort(double[] f, int lo, int hi) { // Indizes lo <= i < hi
        // Initialisiere -
        int N = hi - lo;
        
        // Terminiere (falls trivial) -
        if (N <= 1) return; // Array der Laenge 1 ist bereits sortiert

        // Rekuriere (Teile und Herrsche)
        int mid = lo + N/2; // bestimme die Mitte .. 
        // .. und sortiere sortiere rekursiv ..
        sort(f,lo,mid); // .. linkes und ..
        sort(f,mid,hi); // .. rechtes Teil-Array.
        // Merging: 
        double[] hilfs = new double[N]; 
        // Elemente sortiert in hilfs kopieren:
        int i = lo, j = mid, k; // erste zu betrachtenden Elemente
        for (k = 0; i < mid && j < hi; k++)
            hilfs[k] = f[i] <= f[j] ?
                f[i++] : f[j++]; // das Kleinere gewinnt!
        // "der Gewinner" wird *danach* inkrementiert (Post-Inkrement)
        // nun gilt: i == mid oder j == hi => kopiere noch den Rest
        while ( i < mid)         
            hilfs[k++] = f[i++]; // Inkrement *nach* Zuweisung (Post-Inkrement)
        while ( j < hi) 
            hilfs[k++] = f[j++]; // Inkrement *nach* Zuweisung (Post-Inkrement) 
        for (k = 0; k < N; k++) // alles aus 'hilfs' ..
	    f[lo + k] = hilfs[k];   // .. zurueckkopieren
    }
    private static double timeTrial(int n) {
        double[] a = new double[n];
        for (int i = 0; i < n; i++) {
            a[i] = StdRandom.uniform(2000000) - 1000000;
        }
        Stopwatch s = new Stopwatch();
        sort(a);
        return s.elapsedTime();
    }
    public static void main(String[] args) {
	if ( args.length > 0) {
	    int n = Integer.parseInt(args[0]); 
	    double[] a = new double[n];
	    for (int i = 0; i < n; i++)
		a[i] = StdIn.readDouble();
	    sort(a);
	    for (int i = 0; i < n; i++)
		System.out.println(a[i]);
	}
        else { // Doublingtest
	    System.out.printf("%7s %7s %4s\n", "size", "time", "ratio");
	    double prev = timeTrial(256);
	    int K = 8;
	    int upper = (int) Math.pow(10,K);
	    for (int N = 512; N < upper; N += N) {
		double curr = timeTrial(N);
		System.out.printf("%7d %7.2f %4.2f\n", N, curr, curr / prev);
		prev = curr;
	    } 
	} 	
    }
}
