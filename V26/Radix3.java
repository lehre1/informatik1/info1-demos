public class Radix3 {
   static final int BASE = 256; // probiere 10, 100, 1000, 128, 256
    // maximales K, so dass BASE^K < Integer.MAX_VALUE (== 2^31-1)
    static int K = (int) (31/(Math.log(BASE)/Math.log(2)));

    static int[] power = new int[K+1];
    // statischer Block, wird bei Start der Klasse einmalig ausgeführt
    static {            // nützlich für umfangreichere Initialisierungen
        power[0] = 1;
        for (int i = 1; i <= K; i++) {
            power[i] = power[i-1]*BASE;
        }
    }
    private static int digit(int j, int n) {
        return (n / power[j]) % BASE;
    }
    public static void sort(int[] f) {
	QueueOfInts[] q = new RingbufferOfInts[BASE];
	for (int d = 0; d < BASE; d++) {
	    q[d] = new RingbufferOfInts();
	}
        int N = f.length;
        for (int j = K-1; j >= 0; j--) {
	    for (int i = 0; i < N; i++) 
		q[digit(j,f[i])].enqueue(f[i]);
	    int i = 0;
	    for (int d = 0; d < BASE; d++)
		while (!q[d].empty()) {
		    f[i] = q[d].dequeue();
		    i++;
		}
	}
    }
    private static double timeTrial(int n) {
        int[] a = new int[n];
        int max = 1;
        for (int k = 0; k < K; k++)
            max *= BASE;
        for (int i = 0; i < n; i++) {
            a[i] = StdRandom.uniform(max);
        }
        Stopwatch s = new Stopwatch();
        sort(a);
        return s.elapsedTime();
    }
    public static void main(String[] args) {
        System.out.println("\nBasis: " + BASE);
        System.out.println(); // Leerzeile
        // Doublingtest
        System.out.printf("%7s %7s %4s\n", "size", "time", "ratio");
        double prev = timeTrial(256);
        int upper = (int) Math.pow(BASE,K+1);
        for (int N = 512; N < upper; N += N) {
            double curr = timeTrial(N);
            System.out.printf("%7d %7.2f %4.2f\n", N, curr, curr / prev);
            prev = curr;
        } 
    }
}
