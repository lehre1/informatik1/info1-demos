public class RingbufferOfInts implements QueueOfInts {
    final private  int MAX = 16; // Kapazitaet
    private int[] elements = new int[MAX]; 
    private int count; // Anzahl gespeicherter Werte
    private int first, free; // erste Belegung, naechster freier Platz
    public RingbufferOfInts() {
	first = 0;
	free = 0;
	count = 0;
    }
    private  void resize(int len) { // dynamische Laengenanpassung
	int[] tmp = new int[len]; // durch ..
	for (int i = 0; i < count; i++)  // .. Umspeicherung ..
	    tmp[i] = elements[i];        // .. in neues Feld und ..
	elements = tmp;}                 // .. Bindung an Instanzvariable
    public void enqueue(int i) {
	if ( full()) resize( 2 * elements.length);
	free = (first + count) % elements.length;
	elements[free] = i; 
	free++;
	count++;
    }
    public int dequeue() {
	if (empty())
	    throw new RuntimeException("Pufferunterlauf!");
	int i = elements[first];
        first++;
	first %= elements.length;
	count--;
	return i;
    }
    public boolean empty() { return (count == 0);}
    public boolean full() { return (count == elements.length);}

    public String toString() {
	String t = "";
	for ( int i = count-1; i >= 0; i--)
	    t = elements[ (first + i) % elements.length] + " " + t;
	return "< " + t + "<";
    }
    public static void main( String[] args) { // Test-Unit
	RingbufferOfInts q = new RingbufferOfInts(); 
	int i;
	if (args.length > 0) System.err.println(q);
	while (!StdIn.isEmpty()) {
	    i = StdIn.readInt();
            q.enqueue(i);
	    if (args.length > 0) 
		System.err.println(q);
	}
	while (!q.empty()) {
            System.out.println(q.dequeue());
	    if (args.length > 0) 
		System.err.println(q);
	}
    }
}
