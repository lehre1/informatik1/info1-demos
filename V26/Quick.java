public class Quick {
    public static void sort(double[] f) {
	sort( f, 0, f.length-1);
    }
    public static void sort(double[] f, int lo, int hi) {
	if ( hi <= lo) return;
	int p = partition(f, lo, hi);
	sort(f,  lo, p-1);
	sort(f, p+1,  hi);
    }
    public static int partition(double[] f, int lo, int hi) {
	int i = lo, j = hi+1;
	double v = f[lo], tmp;

	while (true) {
	    while ( f[++i] < v) 
		if ( i == hi) break;
	    while ( v < f[--j]) 
		if ( j == lo) break;
	    if ( i >= j) break;
	    tmp = f[i]; f[i] = f[j]; f[j] = tmp;
	}
	tmp = f[lo]; f[lo] = f[j]; f[j] = tmp;
	return j;
    }

    private static double timeTrial(int n) {
        double[] a = new double[n];
        for (int i = 0; i < n; i++) {
            a[i] = StdRandom.uniform(2000000) - 1000000;
        }
        Stopwatch s = new Stopwatch();
        sort(a);
        return s.elapsedTime();
    }
    public static void main(String[] args) {
	if ( args.length > 0) {
	    int n = Integer.parseInt(args[0]); 
	    double[] a = new double[n];
	    for (int i = 0; i < n; i++)
		a[i] = StdIn.readDouble();
	    sort(a);
	    for (int i = 0; i < n; i++)
		System.out.println(a[i]);
	}
        else { // Doublingtest
	    System.out.printf("%7s %7s %4s\n", "size", "time", "ratio");
	    double prev = timeTrial(256);
	    int K = 8;
	    int upper = (int) Math.pow(10,K);
	    for (int N = 512; N < upper; N += N) {
		double curr = timeTrial(N);
		System.out.printf("%7d %7.2f %4.2f\n", N, curr, curr / prev);
		prev = curr;
	    } 
	} 	
    }
}
