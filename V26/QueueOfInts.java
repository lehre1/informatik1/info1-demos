public interface QueueOfInts {
    // kein Konstruktor!
    public boolean empty();      // true falls nichts gespeichert ist, sonst false
    public void enqueue(int i);  // fuegt i ein
    public int dequeue();       // liefert und entfernt aelteste Referenz
}
