public class Radix2 {
    // Radixsort mit anderer Basis anstelle 10:
    /* Basis: 10
    static int K = 8;  // Stellenzahl
    static int[] power = {
        1,
        10,
        100,
        1000,
        10000,
        100000,
        1000000,
        10000000,
        100000000,
        1000000000};  // Stellenwerte
    */
    ///* Basis: 100
    static int K = 4;  // Stellenzahl
    static int[] power = {
        1,
        100,
        10000,
        1000000,
        100000000};  // Stellenwerte
    //*/
    /* Basis: 1000
    static int K = 3;  // Stellenzahl
    static int[] power = {
        1,
        1000,
        1000000,
        1000000000};  // Stellenwerte
    */

    static final int BASE = power[1];
    // digit(j,n) liefert j-te Ziffer von n (Basis: BASE) 
    private static int digit(int j, int n) {
        return (n / power[j]) % BASE;
    }
    public static void sort(int[] f) {
	QueueOfInts[] q = new RingbufferOfInts[BASE];
	for (int d = 0; d < BASE; d++) {
	    q[d] = new RingbufferOfInts();
	}
        int N = f.length;
        for (int j = K-1; j >= 0; j--) {
	    for (int i = 0; i < N; i++) 
		q[digit(j,f[i])].enqueue(f[i]);
	    int i = 0;
	    for (int d = 0; d < BASE; d++)
		while (!q[d].empty()) {
		    f[i] = q[d].dequeue();
		    i++;
		}
	}
    }
    private static double timeTrial(int n) {
        int[] a = new int[n];
        int max = 1;
        for (int k = 0; k < K; k++)
            max *= BASE;
        for (int i = 0; i < n; i++) {
            a[i] = StdRandom.uniform(max);
        }
        Stopwatch s = new Stopwatch();
        sort(a);
        return s.elapsedTime();
    }
    public static void main(String[] args) {
        if (args.length > 0) {
            int N = Integer.parseInt(args[0]); 
            int[] f = new int[N];
            int t;
            System.out.println(N + 
                               " nichtnegative ganze Zahlen eingeben:");
            for (int i = 0; i < N; i++) {
                do {
                    t = StdIn.readInt();
                    if ( t < 0) System.out.println("ignoriert, da negativ");
                } while (t < 0);
                f[i] = t;
            }
            sort(f);
            for (int i = 0; i < N; i++) {
                System.out.println(f[i]);
            }
            return;
        } 
        else { // Doublingtest
  	    System.out.printf("%7s %7s %4s\n", "size", "time", "ratio");
	    double prev = timeTrial(256);
	    // K = 3;
	    int upper = (int) Math.pow(BASE,K);
	    for (int N = 512; N < upper; N += N) {
		double curr = timeTrial(N);
		System.out.printf("%7d %7.2f %4.2f\n", N, curr, curr / prev);
		prev = curr;
	    } 
        }
    }
}
