public class IsPrime { // Probedivision
    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        if (n<=1) {
            System.out.println("NO");
            return;
        }
        int k;
        for (k = 2; k <= n; i++) {
            if ( n%k == 0)
                break;     // <- sofortiges Verlassen der Schleife
        }
        if (k > n/k)
            System.out.println("YES");
        else
            System.out.println("NO");
    }
}
