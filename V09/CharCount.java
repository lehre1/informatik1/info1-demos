public class CharCount {
    public static void main(String[] args) {
        int M = 256;
	int[] freq = new int[M]; // speichert Häufigkeiten (default-init!)
	char c;
	while (!StdIn.isEmpty()) {
	    c = StdIn.readChar();
	    freq[c]++;
	}
        for (c = 0; c < M; c++) {
	    if (freq[c]>0)
		System.out.println((char) c + " : " + freq[c]);
	}	
    }
}
