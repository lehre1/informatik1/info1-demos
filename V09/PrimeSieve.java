public class PrimeSieve {
  public static void main(String[] args) {
    int n = Integer.parseInt(args[0]);
    boolean[] isPrime = new boolean[n+1];
    for (int k = 2; k <= n; k++)
      isPrime[k] = true;        
    for (int k = 2; k <= n/k; k++) {
      if (isPrime[k]) {
	for ( int j = k; j <= n/k; j++)
	  isPrime[k*j] = false; // Vielfache von Primzahlen sind nicht prim
      }
    }
    int primes = 0;
    for (int k = 2; k <= n; k++) 
        if (isPrime[k]) primes++;  // Primzahlen zaehlen
    System.out.println(primes);    
  }
}
