/* Listet Zeichen nach fallenden Häufigkeiten auf 
   - mit Kdozeilenargument: zusätzlich in ASCII-Reihenfolge zum Testen
*/
public class CharSort {
    public static void main(String[] args) {
	int[] freq = new int[256]; // speichert die Häufigkeiten
	char c;
        int count = 0; // Anzahl verschiedener Zeichen 
	while (!StdIn.isEmpty()) {
	    c = StdIn.readChar();
            if (freq[c]==0) {
                count++;
            }
	    freq[c]++;
	}
        char[] chars = new char[count]; // speichert vorkommende Zeichen
        int n = 0;
        for (c = 0; c < 256; c++) {
	    if (freq[c]>0) {
                chars[n] = c;
                n++;
            }
        }

        if (args.length > 0) {
            System.out.println("Zeichen in ASCII-Reihenfolge:");
            for (c = 0; c < 256; c++) {
                if (freq[c]>0) {
                    System.out.println((char) c + " : " + freq[c]);
                }
            }
        }
        // Bubblesort nach *absteigenden* Häufigkeiten
        boolean changed;
        do {
            changed = false;
            for (int i = 0; i<count-1; i++) {
                if (freq[chars[i]]<freq[chars[i+1]]) {
                    char tmp = chars[i];
                    chars[i]=chars[i+1];
                    chars[i+1]=tmp;
                    changed = true;
                }
            }
        } while (changed);

        System.out.println("Zeichen nach fallenden Häufigkeiten:");
        for (int i = 0; i < count; i++) {
            System.out.println((char) chars[i] + " : " + freq[chars[i]]);
        }
    }
}
