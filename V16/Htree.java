/*
 * nach [Sedgewick/Wayne]
 */

public class Htree {
    public static void draw(int n, double x, double y, double size) {
        if (n == 0) return; // Basisfall
        double x0 = x - size/2, x1 = x + size/2; // Koordinaten .. 
        double y0 = y - size/2, y1 = y + size/2; // .. der 4 Enden
	StdDraw.line( x0, y0, x0,  y1); // linkes Bein und ..
	StdDraw.line( x1, y0, x1,  y1); // .. rechtes Bein vom H 
	StdDraw.line( x0,  y, x1,  y);  // "Balken"
        // zeichne rekursiv 4 kleinere H-Baeume der Ordnung n-1
        draw(n-1, x0, y0, size/2);    // unten links
        draw(n-1, x0, y1, size/2);    // oben links
        draw(n-1, x1, y0, size/2);    // unten rechts
        draw(n-1, x1, y1, size/2);    // oben rechts
    }
    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        double x = 0.5, y = 0.5;   // Mittelpunktkoordinaten
        double size = 0.5;         // Seitenlaenge
        draw(n, x, y, size);
    }
}
