public class Brownian { 
    // Rekursive Mittelpunktsverschiebung
    public static void 
	curve(double x0, double y0, double x1, double y1, double var, double s) {
        if (x1 - x0 < .005) { // Basisfall
            StdDraw.line(x0, y0, x1, y1);
            return;
        }
        double xm = (x0 + x1) / 2; // x- und y-Koordinaten ..
        double ym = (y0 + y1) / 2; // .. des Mittelpunkts
	// Zufaellige Mittelpunktsverschiebung laengs y-Achse:
	double delta = StdRandom.gaussian(0, Math.sqrt(var));
        ym = ym + delta;
        curve(x0, y0, xm, ym, var/s, s); // rekursive Konstruktion der ..
        curve(xm, ym, x1, y1, var/s, s); // .. beiden Kurvenabschnitte.
    } 
    public static void main(String[] args) { 
        double H = Double.parseDouble(args[0]); // Hurst exponent
        double s = Math.pow(2, 2*H);            // s = 2^(2H) 
        curve(0.0, 0.5, 1.0, 0.5, .01, s); 
    } 
} 
