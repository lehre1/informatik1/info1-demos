public class Fibonacci
{
    // Fibonacci-Zahlen rekursiv ..
    public static long rfib(int n) {
	if (n < 2)
	    return n;
	return rfib(n-1) + rfib(n-2);
    }
    // .. memoisiert ..
    // static int N = 100;
    // static long[] f = new long[N]; 
    // public static void mFib() {
    //     f[0] = 0; f[1] = 1; 
    //     for (int i = 2; i < N; ++i)
    //         f[i] = f[i-1] + f[i-2];
    // }
    public static void main(String[] argv) {
        int n = Integer.parseInt(argv[0]);
        // System.out.println("Rekursive Berechnung:");
        for (int i = 0; i <= n; ++i)
            System.out.println(rfib(i));
        // System.out.println("Memoisierte Berechnung:");
        // mFib();
        // for (int i = 0; i < n; ++i)
        //     System.out.println(f[i]);
    }
}

